#+TITLE: DTOS Configs
#+DESCRIPTION: A repository of default configuration files for DTOS.
#+AUTHOR: Derek Taylor (DistroTube)

* What programs use this repo?
The following DTOS packages have their configs hosted here:
+ winkleros-alacritty
+ winkleros-backgrounds
+ winkleros-bash
+ winkleros-conky
+ winkleros-doom
+ winkleros-fish
+ winkleros-local-bin
+ winkleros-opendoas
+ winkleros-sxiv
+ winkleros-xmobar
+ winkleros-xmonad
+ winkleros-xresources
+ winkleros-xwallpaper
+ winkleros-zsh

* Installing these programs
You must have the DTOS core repo enabled on an Arch Linux-based system to install these.  When you install these programs, they don't install executable programs.  These programs are just configuration files.  These configuration files get placed in /etc/winkleros.  For example, if you install winkleros-bash, it places the DTOS .bashrc configuration file in /etc/winkleros.  It doesn't place the .bashrc in your $USER's $HOME. You must manually move it from /etc/winkleros over to your $HOME.

=NOTE:= Most of the winkleros-* packages will install the programs that the configs depend on.  For example, winkleros-fish will also install fish since it is a dependency.  winkleros-alacritty will install the alacritty terminal, etc.
